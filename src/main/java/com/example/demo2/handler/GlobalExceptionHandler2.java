//package com.example.demo2.handler;
//
//import com.example.demo2.response.R;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import org.springframework.web.servlet.NoHandlerFoundException;
//
//
///**
// * @author lxh
// * @ClassName: GlobalExceptionHandler
// * @Description: 简易版全局异常处理器
// * @date 2019/7/30 13:57
// */
//@RestControllerAdvice
//public class GlobalExceptionHandler2 {
//
//    /**
//     * 处理自定义异常
//     */
//    @ExceptionHandler(NoHandlerFoundException.class)
//    public R handleRRException(NoHandlerFoundException e) {
//        return new R(e.getMessage(), e.getMessage());
//    }
//
//}