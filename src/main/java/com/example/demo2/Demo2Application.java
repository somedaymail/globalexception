package com.example.demo2;

import com.example.demo2.enums.ResponseEnum;
import com.example.demo2.response.R;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Demo2Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo2Application.class, args);
    }


    @PostMapping("/test")
    public R<User> get(User user) {
        if (user.getId().equalsIgnoreCase("1")) {
            user = null;
            ResponseEnum.LICENCE_NOT_FOUND.assertNotNull(user);
        }
        return new R(new User());
    }

    @GetMapping("/test")
    public R<User> get2() {
        User user = null;
//        ResponseEnum.LICENCE_NOT_FOUND.assertNotNull(user);
        ResponseEnum.LICENCE_NOT_FOUND.assertNotEmptyWithMsg("","用户为1");
        return new R(new User());
    }
}

