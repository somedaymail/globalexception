package com.example.demo2.response;

import lombok.Data;

import java.util.List;

/**
 * <p></p>
 *
 * @author
 * @date 2019/5/2
 */
@Data
public class QueryData<T> {
    /**
     * 数据列表
     */
    private List<T> records;
    /**
     * 总记录数
     */
    private long totalCount;
    /**
     * 当前页，从1开始
     */
    private long pageNo;
    /**
     * 分页大小
     */
    private long pageSize;

    public QueryData() {

    }


}
