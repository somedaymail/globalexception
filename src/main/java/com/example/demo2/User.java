package com.example.demo2;

import lombok.Data;

/**
 * @author lxh
 * @create 2020-10-19 8:43
 * @desc
 **/
@Data
public class User {

    private String id;
    private String name;


}
